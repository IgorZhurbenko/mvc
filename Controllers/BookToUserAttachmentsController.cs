﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Library;
using Library.Models;

namespace Library.Controllers
{
    public class BookToUserAttachmentsController : Controller
    {
        private readonly LibraryDbContext _context;
        
        public BookToUserAttachmentsController(LibraryDbContext context)
        {
            _context = context;
                
        }

        // GET: BookToUserAttachments
        public async Task<IActionResult> Index()
        {
            //List<BookToUserAttachment> bookToUserAttachments = _context.BookToUserAttachments.ToList();


            return View(new AllAttachments(
                _context.Users.ToList(), 
                _context.Books.ToList(), 
                await _context.BookToUserAttachments.ToListAsync()
                //bookToUserAttachments
                ));
        }

        // GET: BookToUserAttachments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookToUserAttachment = await _context.BookToUserAttachments
                .FirstOrDefaultAsync(m => m.ID == id);
            if (bookToUserAttachment == null)
            {
                return NotFound();
            }

            return View(bookToUserAttachment);
        }

        // GET: BookToUserAttachments/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BookToUserAttachments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BookID,UserID")] BookToUserAttachment bookToUserAttachment)
        {
            if (ModelState.IsValid)
            {
                //if (bookToUserAttachment.AttachmentDate == DateTime.MinValue) { bookToUserAttachment.AttachmentDate = DateTime.Now; }
                _context.Add(bookToUserAttachment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(bookToUserAttachment);
        }

        // GET: BookToUserAttachments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookToUserAttachment = await _context.BookToUserAttachments.FindAsync(id);
            if (bookToUserAttachment == null)
            {
                return NotFound();
            }
            return View(bookToUserAttachment);
        }

        // POST: BookToUserAttachments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,BookID,UserID,AttachmentDate")] BookToUserAttachment bookToUserAttachment)
        {
            if (id != bookToUserAttachment.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bookToUserAttachment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookToUserAttachmentExists(bookToUserAttachment.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bookToUserAttachment);
        }

        // GET: BookToUserAttachments/Delete/5
        public async Task<IActionResult> Delete([Bind("id")]int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookToUserAttachment = await _context.BookToUserAttachments
                .FirstOrDefaultAsync(m => m.ID == id);
            if (bookToUserAttachment == null)
            {
                return NotFound();
            }

            return View(bookToUserAttachment);
        }

        // POST: BookToUserAttachments/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed([Bind("id")]int id)
        {
            var bookToUserAttachment = await _context.BookToUserAttachments.FindAsync(id);
            _context.BookToUserAttachments.Remove(bookToUserAttachment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookToUserAttachmentExists(int id)
        {
            return _context.BookToUserAttachments.Any(e => e.ID == id);
        }
    }
}
