﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Library.Extensions;

namespace Library.Models
{
    public class BookToUserAttachment
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public int BookID { get; set; }

        [Required]
        public int UserID { get; set; }

        //[NotMapped]
        //public DateTime AttachmentDate { get; set; }
        //public string AttachmentDateText { get { return AttachmentDate.ToString(); } set { AttachmentDateText = value; } }
    }

    public struct AttachmentClearData
    {
        public int AttachmentID;
        public string BookName;
        public AttachmentClearData(int attachmentID, string bookName) { this.AttachmentID = attachmentID; this.BookName = bookName; }
    }

    public struct AllAttachments
    {
        public AllAttachments(List<User> allUsers, List<Book> allBooks, List<BookToUserAttachment> attachments)
        {
            this.AllUsers = allUsers;
            this.AllBooksList = allBooks;
            this.Attachments = attachments;
            this.UserID = 0; this.BookID = 0;
            this.SortedBooks = new Dictionary<User, List<Book>>();
            this.SortedAttachments = new Dictionary<User, List<AttachmentClearData>>();

            this.SortedBooks = SortBooksByUsers();
            this.SortedAttachments = SortAttachmentsByUsers();
        }
        public List<BookToUserAttachment> Attachments;
        public List<User> AllUsers { get; set; }
        public List<Book> AllBooksList { get; set; }

        public Dictionary<User, List<AttachmentClearData>> SortedAttachments;


        public int UserID;
        public int BookID;

        public Book GetBookByID(int BookId)
        {
            return AllBooksList.Find(b => b.ID == BookId);
        }
        public User GetUserByID(int BookId)
        {
            return AllUsers.Find(b => b.ID == BookId);
        }

        public Dictionary<User, List<Book>> SortedBooks { get; set; }

        public Dictionary<User, List<AttachmentClearData>> SortAttachmentsByUsers()
        {
            Dictionary<User, List<AttachmentClearData>> result = new Dictionary<User, List<AttachmentClearData>>();
            foreach (User user in AllUsers) { result.Add(user, new List<AttachmentClearData>()); }
            foreach (BookToUserAttachment attachment in this.Attachments)
            {
                result[GetUserByID(attachment.UserID)].Add(
                    new AttachmentClearData(attachment.ID, GetBookByID(attachment.BookID).Name)
                    );
            }
            return result;
        }

        public Dictionary<User, List<Book>> SortBooksByUsers()
        {
            Dictionary<User, List<Book>> result = new Dictionary<User, List<Book>>();
            foreach (User user in AllUsers) { result.Add(user, new List<Book>()); }
            foreach (BookToUserAttachment attachment in this.Attachments)
            {
                result[GetUserByID(attachment.UserID)].Add(
                    GetBookByID(attachment.BookID)
                    );
            }
            return result;
        }


        public Dictionary<User, List<AttachmentClearData>> GetDataForViewGridForAttachments(int MaxBooksNumberForOneUser = 4)
        {
            foreach (KeyValuePair<User, List<AttachmentClearData>> UserAttachment in SortedAttachments)
            {
                if (UserAttachment.Value.Count() < MaxBooksNumberForOneUser) { UserAttachment.Value.Add(new AttachmentClearData(0, "+")); }
            }

            return SortedAttachments;
        }


    }
}
