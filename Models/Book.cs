﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    public class Book
    {
        public int ID { get; set; }
        
        [Required]
        [MaxLength(40)]
        public string Name { get; set; }
        public string Author { get; set; }

        public string RegistrationDate { get; set; }

    }
}
