﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    public class Registration
    {
        [Required(ErrorMessage = "Email is not provided.")]
        [Key]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is not provided.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Wrong password.")]
        public string ConfirmPassword { get; set; }
    }
}
