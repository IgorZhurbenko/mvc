﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Email not provided.")]
        [Key]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password not provided")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
