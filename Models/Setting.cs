﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using SQLite;

namespace Library.Models
{
    public class Setting
    {
        public int ID { get; set; }

        [Key]
        public string SettingName { get; set; }
        public int NumericValue { get; set; }
        public string StringValue { get; set; }
        public bool LogicalValue { get; set; }
    }
}
