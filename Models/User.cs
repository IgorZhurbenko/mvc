﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class User
    {
        public int ID { get; set; }
        
        [Required]
        public string Name { get; set; }
        public string PhoneNumber { get; set; }

        
        public string RegistrationDate { get; set; }
        [NotMapped]
        public List<Book> ReadBooks { get; set; }
    }
}
