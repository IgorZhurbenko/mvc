﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;


namespace Library
{
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext()
        { 
         //   this.Database.EnsureCreated(); 
        }
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options)
            :base(options)
        {
            this.Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder DbOptions)
        {
           // DbOptions.UseSqlite(connectionString: "FileName=./MainDatabase.db3");
        }
        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<BookToUserAttachment> BookToUserAttachments { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<Library.Models.Login> Login { get; set; }
        public DbSet<Library.Models.Setting> Settings { get; set; }
        
        
    }
}
